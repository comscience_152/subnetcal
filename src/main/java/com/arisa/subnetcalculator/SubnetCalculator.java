/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.arisa.subnetcalculator;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author Asus
 */
public class SubnetCalculator {

     private String ipAddress;
    private String subnetMask;
    private int subnetBits;
    private long maxSubnets;
    private long hostsPerSubnet;
    private long networkAddress;
    private long broadcastAddress;
    private long firstHostAddress;
    private long lastHostAddress;
    private String subnetID;
    private String hexIPAddress;
    private String wildcardMask;
    private String subnetBitmap;
    private String maskBits;
    private String firstOctetRange;
    private String prefix;

    public SubnetCalculator(String ipAddress, String subnetMask) {
        this.ipAddress = ipAddress;
        this.subnetMask = subnetMask;
        calculateSubnetDetails();
    }

    private boolean isValidIPAddress(String ipAddress) {
        String ipv4Pattern = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$";
        return Pattern.matches(ipv4Pattern, ipAddress);
    }

    private boolean isValidSubnetMask(String subnetMask) {
        String subnetPattern = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$";
        return Pattern.matches(subnetPattern, subnetMask);
    }

    private void calculateSubnetDetails() {
        if (isValidIPAddress(ipAddress) && isValidSubnetMask(subnetMask)) {
            String[] ipParts = ipAddress.split("\\.");
            String[] maskParts = subnetMask.split("\\.");
            long ip = 0;
            long mask = 0;

            for (int i = 0; i < 4; i++) {
                ip |= Long.parseLong(ipParts[i]) << (24 - 8 * i);
                mask |= Long.parseLong(maskParts[i]) << (24 - 8 * i);
            }

            subnetBits = Long.bitCount(mask);
            maxSubnets = 1L << (32 - subnetBits);
            hostsPerSubnet = (1L << (32 - subnetBits)) - 2;
            long step = (1L << (32 - subnetBits));

            networkAddress = ip & mask;
            broadcastAddress = networkAddress + step - 1;

            firstHostAddress = networkAddress + 1;
            lastHostAddress = broadcastAddress - 1;

            subnetID = longToIp(networkAddress);
            hexIPAddress = longToHex(ip);
            wildcardMask = calculateWildcardMask(mask);
            firstOctetRange = calculateFirstOctetRange(ip, mask);
            subnetBitmap = calculateSubnetBitmap(mask);
            maskBits = calculateMaskBits(mask);

        } else {
            System.out.println("Invalid IP address or subnet mask format.");
            System.exit(1);
        }
    }

    private String longToIp(long ip) {
        return String.format("%d.%d.%d.%d",
                (ip >> 24) & 0xFF,
                (ip >> 16) & 0xFF,
                (ip >> 8) & 0xFF,
                ip & 0xFF);
    }

    private String longToHex(long ip) {
        return String.format("%02X:%02X:%02X:%02X",
                (ip >> 24) & 0xFF,
                (ip >> 16) & 0xFF,
                (ip >> 8) & 0xFF,
                ip & 0xFF);
    }

    private String calculateWildcardMask(long mask) {
        long wildcard = 0xFFFFFFFFL - mask;
        return longToIp(wildcard);
    }

    private String calculateSubnetBitmap(long mask) {
        StringBuilder subnetBitmap = new StringBuilder(prefix);
        long bit = 1L << 30;
        if (prefix == "0") {
            for (int i = 1; i < 32; i++) {
                if (i % 8 == 0 && i != 0) {
                    subnetBitmap.append(".");
                }
                if ((mask & bit) != 0) {
                    subnetBitmap.append("n");
                } else {
                    subnetBitmap.append("h");
                }
                bit >>= 1;
            }
        } else if (prefix == "10") {
            for (int i = 1; i < 32; i++) {
                if (i % 8 == 0 && i != 0) {
                    subnetBitmap.append(".");
                }
                if ((mask & bit) != 0) {
                    subnetBitmap.append("n");
                    if (i == 4) {
                        subnetBitmap.deleteCharAt(4);
                    }
                } else {
                    subnetBitmap.append("h");
                }
                bit >>= 1;
            }
        } else if (prefix == "110") {
            for (int i = 1; i < 32; i++) {
                if (i % 8 == 0 && i != 0) {
                    subnetBitmap.append(".");
                }
                if ((mask & bit) != 0) {
                    subnetBitmap.append("n");
                    if (i == 4) {
                        subnetBitmap.deleteCharAt(4);
                    }
                    if (i == 5) {
                        subnetBitmap.deleteCharAt(5);
                    }
                } else {
                    subnetBitmap.append("h");
                }
                bit >>= 1;
            }
        }
        return subnetBitmap.toString();
    }

    private String calculateMaskBits(long mask) {
        int maskBitCount = 0;
        long bit = 1L << 31;
        while (maskBitCount < 32 && (mask & bit) != 0) {
            maskBitCount++;
            bit >>= 1;
        }
        return Integer.toString(maskBitCount);
    }

    private String calculateFirstOctetRange(long ip, long mask) {
        long firstOctet = (ip & mask) >> 24;
        if (firstOctet >= 1 && firstOctet <= 126) {
            prefix = "0";
            return "1 - 126";
        } else if (firstOctet >= 128 && firstOctet <= 191) {
            prefix = "10";
            return "128 - 191";
        } else if (firstOctet >= 192 && firstOctet <= 223) {
            prefix = "110";
            return "192 - 223";
        } else if (firstOctet >= 224 && firstOctet <= 239) {
            return "Multicast";
        } else if (firstOctet >= 240 && firstOctet <= 255) {
            return "Reserved";
        } else {
            return "Not in the defined IP address classes";
        }
    }

    public void displaySubnetDetails() {
        System.out.println("IP Address: " + ipAddress);
        System.out.println("Hex IP Address: " + hexIPAddress);
        System.out.println("Subnet Mask: " + subnetMask);
        System.out.println("Subnet Bits: " + subnetBits);
        System.out.println("Maximum Subnets: " + maxSubnets);
        System.out.println("Hosts per Subnet: " + hostsPerSubnet);
        System.out.println("Subnet ID: " + subnetID);
        System.out.println("Host Address Range: " + longToIp(firstHostAddress) + " - " + longToIp(lastHostAddress));
        System.out.println("Broadcast Address: " + longToIp(broadcastAddress));
        System.out.println("Wildcard Mask: " + wildcardMask);
        System.out.println("Subnet Bitmap: " + subnetBitmap);
        System.out.println("Mask Bits: " + maskBits);
        System.out.println("First Octet Range: " + firstOctetRange);
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.print("Enter IP address: ");
        String ipAddress = kb.next();
        System.out.print("Enter subnet mask: ");
        String subnetMask = kb.next();

        SubnetCalculator network = new SubnetCalculator(ipAddress, subnetMask);
        network.displaySubnetDetails();
        kb.close();
    }
}
